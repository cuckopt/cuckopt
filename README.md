# A Optimizing the assignment of 25-mers of the E. coli genome
We here provide an example showing how to optimize the assignment of k-mers of genomes.
Our implementation is provided as a Python package. It is currently restricted to h = 3 hash
functions. We recommend to use conda to manage Python packages and separate environments
for each application.

## Installing Miniconda. 
Go to https://docs.conda.io/en/latest/miniconda.html and down-
load the Miniconda installer: Choose Python 3.7 (or higher), your operating system, and preferably
the 64 bit version. Follow the instructions of the installer and append the conda executable
to your PATH (even if the installer does not recommend it). You can let the installer do it,
or do it manually by editing your .bashrc or similar file under Linux or MacOS, or editing
the environment varialbes under Windows. To verify that the installation works, open a new
terminal and execute
```
conda --version # ideally 4.5.xx or higher
python --version # ideally 3.7.xx or higher
```
## Obtain our software. 
Our software is obtained by cloning a public git repository:
```
git clone https://gitlab.com/cuckopt/cuckopt.git
```

## Create and activate new conda environment.
To run our software, a conda environment
with the required libraries needs to be created. A list of needed libraries is provided in the
requirements.txt file in the cloned repository; it can be used to create a new environment:
```
cd cuckopt # the directory of the cloned repository
conda create --name cuckopt --file requirements.txt
```
This may take some time, as conda searches and downloads packages. After all dependencies
are resolved, you activate the environment and install the package from the repository into this
environment:
```
conda activate cuckopt # activate environment
python setup.py develop # install package into environment
```
The latter command has to be executed from the root directory of the cloned repository (where
setup.py as well as requirements.txt are found).
You now have access to the executables fasta2kmerh5 to convert genomes in FASTA format
to encoded canonical k-mers in HDF5 format, which is the input format for our optimization
tool, and cuckopt to perform the optimization. You will first need a genomic FASTA file.

## Download genomes
The links for the genome files we used in our benchmarks are given in
table 3. To download the FASTA file for the E. coli genome, change to an appropriate data
directory where you will store the genomes and k-mers and download the file. We recommend to
rename the (compressed) FASTA file to someting shorter:
```
cd ~/data/genomes # or somewhere else, may need to be created
wget ftp://ftp.ensemblgenomes.org/pub/bacteria/release-41/fasta/bacteria_91_collection/
escherichia_coli/dna/Escherichia_coli.HUSEC2011CHR1.dna_sm.toplevel.fa.gz
mv Escherichia_coli.HUSEC2011CHR1.dna_sm.toplevel.fa.gz ecoli.fa.gz
```
Alternatively, use curl instead of wget, especially on a Mac, directly specifying the file name.
```
curl ftp://ftp.ensemblgenomes.org/pub/bacteria/release-41/fasta/bacteria_91_collection/
escherichia_coli/dna/Escherichia_coli.HUSEC2011CHR1.dna_sm.toplevel.fa.gz > ecoli.fa.gz
```

## Convert a FASTA genome to HDF5 input. 
Before we can calculate an optimal assignment for
genomic k-mers in a FASTA file, we have to obtain a bit-packed encoding of all unique canonical
k-mers using the fasta2kmerh5 tool. To obtain the HDF5 file for the E. coli genome:
```
fasta2kmerh5 ecoli.fa.gz --out ecoli.h5
```
The tool requires a (gzipped) FASTA file as input an additionally has the following parameters:

`--out FILE` names the output HDF5 file which contains all unique k-mer encodings, should
have extension .h5,

`-k LENGTH` defines the length k of the k-mers (default 25; must satisfy 1 ≤ k ≤ 32)

`--size SIZE` describes the number of k-mers that are expected to be in the FASTA file. If not
given, it will start with 6 million (sufficient for E. coli ) and expand if necessary.

`--rcmode` controls how k-mers encodings are canonicalized; valid values are f, r, both, min,
max; default is max.
The --rcmode parameter deserves an additional explanation. DNA is a double stranded
molecule, and a DNA sequence is equivalent to its reverse complement, obtained by reversing
the sequence and replacing A &lrarr; T and C &lrarr; G. The DNA alphabet being {A,C,G,T}, a DNA
k-mer can be base-4 encoded in 2k bits. To represent a k-mer and its reverse complement by the
same integer code, we take the maximum of the base-4 encoding of the k-mer and its reverse
complement (--rcmode max). This makes the first and last bits less equidistributed than the
middle bits (in fact, 2k − 1 bits would now suffice for odd k), and therefore the hash functions we
use cyclically shift the resulting maxima by k bits to have more equidistributed high-order bits.

## Calculate an optimal assignment. Now 
that we have the input HDF5 file, we need to specify
the parameters for the hash table. To calculate the optimal assignments for E. coli with a bucket
size of b = 4 and a load factor of ρ = 0.9, using the three hash functions linear7387, linear9581,
and linear7153, run:
```
cuckopt ecoli.h5 --bucketsize 4 --load 0.9 --hashfunctions linear7387:linear9581:linear7153
```
This should take a few seconds. Because the --output parameter was not specified, the resulting
assignment will not be saved, but the software will print out relevant information, such as the
time needed for the optimization, the final bucket fill distribution, the assignment distribution
and the optimal cost.
The cuckopt tool requires a HDF5 file with keys as input (as produced by fasta2kmerh5 and
has the following additional parameters:

`--bucketsize SIZE` specifies the bucket size b.

`--load LOAD` specifies the load factor, and must be between 0.01 and 1.

`--hashfunctions STRING` specifies the three hash functions, currently limited to linear hash func-
tions given as a colon-separated string of the form linear7387:linear9581:linear7153.

`--output FILE` specifies an HDF5 output file which stores the k-mers and the computed optimal
assignment.
Please ensure that you have a server with sufficient RAM to optimize the assignment for larger
genomes. For the fruit fly D. melanogaster, 4 GB should suffice. For the human genome, over
40 GB are required, so a machine with at least 64 GB is recommended.
