import sys
import argparse
import datetime
from math import ceil

import numpy as np
from numba import njit, uint64, int64

from .intbitarray import intbitarray
from .dnaio import fasta_reads
from .dnaencode import dna_to_2bits
from .build import generate_kmer_iterator
from .h5utils import save_to_h5group


DEFAULT_SIZE = 5_000_000

def get_argument_parser():
    p = argparse.ArgumentParser()
    p.add_argument('fasta', nargs='+',
        help="path(s) to FASTA file(s) with DNA sequences")
    p.add_argument("--out", metavar="OUT_HDF5", required=True,
        help="output HDF5 file with (canonical) k-mer codes")
    p.add_argument('-k', metavar='INT', type=int, default=25,
        help="k-mer length k <= 32 (default: 25)")
    p.add_argument('--size', '-s', metavar="INT", type=int, default=DEFAULT_SIZE,
        help=f"initial size of array for k-mer codes ({DEFAULT_SIZE}, expanded when necessary)")
    p.add_argument("--rcmode", metavar="MODE", default="max",
        choices=("f", "r", "both", "min", "max"),
        help="mode specifying how to encode k-mers")
    return p


def collect_from_fastas(fastas, shp, rcmode, size=DEFAULT_SIZE):
    k, kmers = generate_kmer_iterator(shp, rcmode)
    
    @njit(nogil=True, locals=dict(code=uint64, i=int64))
    def add_kmers(array, i, sequence):
        for code in kmers(sequence, 0, len(sequence)):
            array[i] = code
            i += 1
        return i
    
    a = np.empty(size, dtype=np.uint64)
    idx = 0  # where to write into a
    for fasta in fastas:
        print(f"# processing '{fasta}'...")
        for header, seq in fasta_reads(fasta):
            n = len(seq)
            while idx + n >= size:
                size = max(idx+n, int(1.6*size))  # expand array
                ax = np.empty(size, dtype=np.uint64)
                ax[:idx] = a[:idx]
                a = ax
            sq = dna_to_2bits(seq)
            idx = add_kmers(a, idx, sq)  # add at most n k-mers into a
    return a[:idx]


def compact_array(uniq, codes):
    width = codes.width
    n = len(uniq)
    limit = 2**width
    codeset = codes.set
    
    @njit(nogil=True, locals=dict(c=uint64))
    def _do_compaction(uniq, codes):
        for i in range(n):
            c = uniq[i]
            assert 0 <= c < limit
            codeset(codes, i, c)

    _do_compaction(uniq, codes.array)
    return n


def main():
    """main method for reading k-mers from FASTA"""
    p = get_argument_parser()
    args = p.parse_args()
    # args.fasta (list[str]), args.out (str), 
    # args.k (int), args.rcmode (str), args.size (int)
    k = args.k
    rcmode = args.rcmode
    if k < 1 or k > 32:
        raise RuntimeError("only k=1..32 is supported")
    exitcode = 0

    starttime = datetime.datetime.now()
    print(f"# {starttime:%Y-%m-%d %H:%M:%S}: sorting {k}-mers (mode={rcmode}) from",
        ", ".join(args.fasta), f"-> {args.out}")
    unsorted = collect_from_fastas(args.fasta, k, rcmode, size=args.size)
    print(f"# collected {len(unsorted)} {k}-mers; sorting...")
    uniq = np.unique(unsorted)
    print(f"# found {len(uniq)} unique {k}-mers; compacting...")

    # bit-compactify uniq into codes (in-place)
    codes = intbitarray(len(uniq), 2*k, init=uniq)
    n = compact_array(uniq, codes)
    assert n == len(uniq)
    m = int(ceil(n*2*k / 64))
    kmercodes = codes.array[:m]

    print(f"# writing {len(uniq)} compacted {2*k}-bit values to HDF5 file {args.out}...")
    outfile = args.out
    info = dict(k=k, kmers=n)
    save_to_h5group(outfile, "info", info=info)
    save_to_h5group(outfile, "data", kmercodes=kmercodes)
    
    endtime = datetime.datetime.now()
    elapsed = (endtime - starttime).total_seconds()
    print(f"# {endtime:%Y-%m-%d %H:%M:%S}: done; this took {elapsed:.1f} seconds.")
    sys.exit(1 if exitcode>0 else 0)


if __name__ == "__main__":
    main()

