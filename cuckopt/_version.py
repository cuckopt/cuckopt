VERSION = "0.9"
__version__ = VERSION

DESCRIPTION = """
The cuckopt package is a tool for computing cost-optimal assignments of element keys to hash functions in (h,b) Cuckoo hashing with h hash functions and buckets of size b.

The current implementation is limited to h=3.

It specifically supports genomic datasets in FASTA format by providing a tool fasta2kmerh5 for converting a genomic FASTA file into a set of unique canonical k-mers to be processed with the cuckopt tool.
"""
