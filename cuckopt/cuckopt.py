from argparse import ArgumentParser
import gc
import datetime

import numpy as np
from numba import njit, int64, uint64

from .hashfunctions import build_get_page_fpr, get_npages
from .build import generate_kmer_iterator
from .h5utils import load_from_h5group, save_to_h5group
from .bitarray import bitarray
from .intbitarray import intbitarray, IntBitArray
from .iter_pages import optimize


def get_argument_parser():
    p = ArgumentParser()
    p.add_argument("--hashfunctions", "-f", nargs='+')#, required=True)
    p.add_argument("--bucketsize", "-b", type=int)#, required=True)
    p.add_argument("--fillratio", "--load", "-fr", type=float)#, required=True)
    p.add_argument("h5",
        help="input HDF5 file with keys")
    # p.add_argument("--output", "-o", help="output file name")
    return p

def _get_hashfunctions(hashfunctions, npages, universe):
    get_pages_fprs = []
    get_keys = []
    if len(hashfunctions) == 1:
        hashfunctions = hashfunctions[0].split(":")
        
    for i in hashfunctions:
        get_page_fpr, get_key = build_get_page_fpr(name=i, universe=universe, npages=npages)
        get_pages_fprs.append(get_page_fpr)
        get_keys.append(get_keys)
        
    return get_pages_fprs, get_keys
       

def checkCoices(choices, kmer_pages, npages, bucketsize):
    pF = np.zeros(npages, dtype=np.uint8)
    for i in range(len(choices)):
        pF[kmer_pages[i][choices[i]]] += 1
        assert(pF[kmer_pages[i][choices[i]]] <= bucketsize)
        
    for i in range(len(choices)):
        if [choices[i]] == 1:
            assert(pF[kmer_pages[i][choices[0]]]<bucketsize)
        if [choices[i]] == 2:
            assert(pF[kmer_pages[i][choices[0]]]<bucketsize)
            assert(pF[kmer_pages[i][choices[1]]]<bucketsize)
    return
    

def dumph5file(fname, choices):
    print(f"updating choices in '{fname}'")
    save_to_h5group(fname, "data", choices=choices)


def main():
    p = get_argument_parser()
    args = p.parse_args()

    print(f"'{datetime.datetime.now()}': read h5 file")
    info = load_from_h5group(args.h5, "info")["info"]
    data = load_from_h5group(args.h5, "data")
    print(info)
    if "hashfuncs" in info:
        hashfuncs = [info["hashfuncs"].decode()]
    else:
        hashfuncs = args.hashfunctions
        

    if "pagesize" in info:
        args.bucketsize = info["pagesize"]
    k = int(info['k'])
    kmers = data['kmercodes']
    nkmers = info['kmers']
    bucketsize = args.bucketsize
    if "npages" in info:
        npages = info["npages"]
    else:
        fillrate = args.fillratio
        npages = get_npages(nkmers, bucketsize, fillrate)
    info['bucketsize'] = bucketsize
    info['npages'] = npages
    info['hashfuncs'] = ":".join(hashfuncs).encode()
    universe = 4**k
    get_pages_fprs, get_keys = _get_hashfunctions(hashfuncs, npages, universe)

    print("INFO")
    print(info)
    
    ckmers = intbitarray(nkmers, 2*k, init=kmers)
    del(data)
    gc.collect()
    gpf = tuple(get_pages_fprs)
    print(f"'{datetime.datetime.now()}':Start optimization")
    choices = optimize(nkmers, bucketsize, npages, gpf, ckmers, k)
    dumph5file(args.h5, choices)


if __name__ == "__main__":
    main()
